﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatforms : MonoBehaviour
{
    #region Fields
    public float platformSpeedPerSecond = -10;
    public bool moveInLoop;
    public float zAxisDisappearingPointValue = -4.2f;
    List<PlatformBlock> platformBlockList;
    List<Vector3> platformBlocksStartPosition;
    #endregion

    #region Methods
    void Start()
    {
        platformBlockList = new List<PlatformBlock>();
        PlatformBlock[] platformBlockArray = GetComponentsInChildren<PlatformBlock>();
        platformBlockList.AddRange(platformBlockArray);
        platformBlocksStartPosition = new List<Vector3>();
        foreach (PlatformBlock p in platformBlockList)
            platformBlocksStartPosition.Add(p.transform.position);
        LevelManager.Instance.resetEvent += Reset;
    }

    void FixedUpdate()
    {
        if (!LevelManager.Instance.IsPausing)
        {
            MovePlatforms();
            if (moveInLoop)
                ReplacePlatformsInLoop();
        }
    }

    void MovePlatforms()
    {
        foreach(PlatformBlock p in platformBlockList)
        {
            p.transform.position = new Vector3(p.transform.position.x, p.transform.position.y,
                p.transform.position.z + (platformSpeedPerSecond * Time.fixedDeltaTime));
        }
    }

    void ReplacePlatformsInLoop()
    {
        foreach(PlatformBlock p in platformBlockList)
        {
            if(p.endPoint.transform.position.z <= zAxisDisappearingPointValue)
            {
                float lastZAxisPoint = 0;
                for(int i = platformBlockList.Count - 1; i >= 0; i--)
                {
                    if (platformBlockList[i].endPoint.transform.position.z > lastZAxisPoint)
                        lastZAxisPoint = platformBlockList[i].endPoint.transform.position.z;
                }
                // move platform block to the end
                p.transform.position = 
                    new Vector3(p.transform.position.x, p.transform.position.y, lastZAxisPoint);
            }
        }
    }

    public void Reset()
    {
        for (int i = 0; i < platformBlockList.Count; i++)
            platformBlockList[i].transform.position = platformBlocksStartPosition[i];
    }

    void OnDestroy()
    {
        LevelManager.Instance.resetEvent -= Reset;
    }
    #endregion
}
