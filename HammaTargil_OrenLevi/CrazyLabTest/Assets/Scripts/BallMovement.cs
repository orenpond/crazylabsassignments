﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    #region Fields
    Camera _camera;
    Rigidbody rb;
    SphereCollider sphereCollider;
    public ControlMethod controlMethod;
    public bool threeSwipeEnableOnGroundOnly;
    public float threeSwipeOnGroundOnlySensitivity = 0.4f; 
    public float horizontalMoveSpeed = 0.7f; //recommended FollowSwipe - 0.7f | ThreeSwipe - 13f 
    public float jumpMagnitude = 7;
    public float gravityMultiplerUpwards = 1;
    public float gravityMultiplerDownwards = 1.1f;
    float addGravityUpwards;
    float addGravityDownwards;
    bool isOnGround;
    float skinwidth = 0.01f;
    LayerMask platfomLayerMask;

    public float rightXPosition = 5;
    public float slideSensitivity = 0.1f;
    Vector3 touchPos;
    Vector3 currentTouchPos;
    Vector3 lastTouchPos;
    bool startedSliding = false;
    public float threeSwipeXBallLeftPosition = -3;
    public float threeSwipeXBallRightPosition = 3;
    Vector3 threeSwipeMoveDestination;
    Vector2 moveXYDirection;
    Animator animator;
    bool changedPauseStatus;
    bool changedResumeStatus;
    Vector3 savedVelocity;
    Vector3 savedAngularVelocity;
    float timePassedInTheAir;
    MeshRenderer myRenderer;
    Material myMaterial;
    public Color materialColorNormal;
    public Color materialColorAbleToSwap;
    Vector3 startPosition;
    bool raisedGameOver;
    public GameObject explosionEffect;

    // For Debug
    public UnityEngine.UI.Text jumpTimeValueText;
    bool setJumpTimeValue = false;
    #endregion

    #region Enums
    public enum ControlMethod { FollowSwipe, ThreeSwipe };
    #endregion

    #region Methods
    void Start()
    {
        _camera = LevelManager.Instance.mainCamera;
        rb = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
        platfomLayerMask = 1 << LayerMask.NameToLayer("Platforms");
        timePassedInTheAir = 0;
        animator = GetComponent<Animator>();
        changedPauseStatus = false;
        changedResumeStatus = true;
        moveXYDirection = Vector2.zero;
        myRenderer = GetComponent<MeshRenderer>();
        myMaterial = myRenderer.material;
        LevelManager.Instance.resetEvent += Reset;
        startPosition = transform.position;
        raisedGameOver = false;
    }

    void Update()
    {
        if (!LevelManager.Instance.IsPausing)
        {
            if(controlMethod == ControlMethod.FollowSwipe)
                HandleFollowSwipeControls();
            if (controlMethod == ControlMethod.ThreeSwipe)
            {
                myMaterial.color = materialColorNormal;
                if (threeSwipeEnableOnGroundOnly)
                {
                    if ((isOnGround || timePassedInTheAir < threeSwipeOnGroundOnlySensitivity)
                        && moveXYDirection == Vector2.zero)
                    {
                        HandleThreeSwipeControls();
                        myMaterial.color = materialColorAbleToSwap;
                    }                      
                }
                else
                    HandleThreeSwipeControls();
            }
        }
    }

    void HandleFollowSwipeControls()
    {
        touchPos = _camera.ScreenToViewportPoint(Input.mousePosition);
        if (touchPos.y < LevelManager.Instance.ViewportGuiStartYPoint)
        {
            if (Input.GetMouseButtonDown(0))
            {
                currentTouchPos = new Vector3(Mathf.Lerp(-rightXPosition, rightXPosition, touchPos.x), 0, 0);
                lastTouchPos = currentTouchPos;
            }
            else if (Input.GetMouseButton(0))
            {
                if (Mathf.Abs(lastTouchPos.x - currentTouchPos.x) > slideSensitivity && !startedSliding)
                    startedSliding = true;
                lastTouchPos = currentTouchPos;
                currentTouchPos = new Vector3(Mathf.Lerp(-rightXPosition, rightXPosition, touchPos.x), 0, 0);
            }
            if (Input.GetMouseButtonUp(0))
            {
                startedSliding = false;
            }
        }
    }

    void HandleThreeSwipeControls()
    {
        touchPos = _camera.ScreenToViewportPoint(Input.mousePosition);
        if (touchPos.y < LevelManager.Instance.ViewportGuiStartYPoint)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastTouchPos = _camera.ScreenToViewportPoint(Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0))
            {
                currentTouchPos = _camera.ScreenToViewportPoint(Input.mousePosition);
                if (Mathf.Abs(lastTouchPos.x - touchPos.x) > slideSensitivity)
                {
                    if (currentTouchPos.x > lastTouchPos.x)
                        moveXYDirection = Vector2.right;
                    else
                        moveXYDirection = Vector2.left;
                }
                else
                    moveXYDirection = Vector2.zero;
            }
        }
    }

    void FixedUpdate()
    {
        if (!LevelManager.Instance.IsPausing)
        {
            if(!changedResumeStatus)
                Resume();

            CheckIsOnGround();
            MoveSideWays();
            if (isOnGround)
            {
                addGravityUpwards = 0;
                addGravityDownwards = 0;
                Jump();
            }
            else
            {
                AddGravity();
            }
            SetJumpSpeed();
            if (transform.position.y < -1f)
            {
                if (!raisedGameOver)
                {
                    raisedGameOver = true;
                    LevelManager.Instance.RaiseGameOver();
                }
            }
        }
        else
        {
            if (!changedPauseStatus)
                Pause();
        }
    }

    void AddGravity()
    {
        if (rb.velocity.y > 0)
        {
            addGravityUpwards += gravityMultiplerUpwards;
            rb.AddForce(Vector3.down * addGravityUpwards, ForceMode.Acceleration);
        }
        else
        {
            addGravityDownwards += gravityMultiplerDownwards;
            rb.AddForce(Vector3.down * addGravityDownwards, ForceMode.Acceleration);
        }
    }

    void MoveSideWays()
    {
        if (controlMethod == ControlMethod.FollowSwipe)
            MoveSideWaysFollowSwipe();
        if (controlMethod == ControlMethod.ThreeSwipe)
            MoveSideWaysThreeSwipe();
    }

    void MoveSideWaysFollowSwipe()
    {
        if (startedSliding)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(currentTouchPos.x, transform.position.y, transform.position.z), horizontalMoveSpeed);
        }
    }

    void MoveSideWaysThreeSwipe()
    {
        if (moveXYDirection == Vector2.left)
        {
            if (transform.position.x > threeSwipeXBallLeftPosition)
            {
                if (transform.position.x > 0)
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        new Vector3(0, transform.position.y, transform.position.z),
                        horizontalMoveSpeed * Time.fixedDeltaTime);
                    if (transform.position.x <= 0)
                    {
                        moveXYDirection = Vector2.zero;
                        transform.position =
                            new Vector3(0, transform.position.y, transform.position.z);
                    }
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        new Vector3(threeSwipeXBallLeftPosition, transform.position.y, transform.position.z),
                        horizontalMoveSpeed * Time.fixedDeltaTime);
                    if (transform.position.x == threeSwipeXBallLeftPosition)
                    {
                        moveXYDirection = Vector2.zero;
                        transform.position =
                            new Vector3(threeSwipeXBallLeftPosition, transform.position.y, transform.position.z);
                    }
                }
            }
            else
            {
                if(transform.position.x == threeSwipeXBallLeftPosition)
                    moveXYDirection = Vector2.zero;
            }
        }
        if (moveXYDirection == Vector2.right)
        {
            if (transform.position.x < threeSwipeXBallRightPosition)
            {
                if (transform.position.x < 0)
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        new Vector3(0, transform.position.y, transform.position.z),
                        horizontalMoveSpeed * Time.fixedDeltaTime);
                    if (transform.position.x >= 0)
                    {
                        moveXYDirection = Vector2.zero;
                        transform.position =
                            new Vector3(0, transform.position.y, transform.position.z);
                    }
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        new Vector3(threeSwipeXBallRightPosition, transform.position.y, transform.position.z),
                        horizontalMoveSpeed * Time.fixedDeltaTime);
                    if (transform.position.x >= threeSwipeXBallRightPosition)
                    {
                        moveXYDirection = Vector2.zero;
                        transform.position =
                            new Vector3(threeSwipeXBallRightPosition, transform.position.y, transform.position.z);
                    }
                }
            }
            else
            {
                if (transform.position.x == threeSwipeXBallRightPosition)
                    moveXYDirection = Vector2.zero;
            }
        }
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
    }

    void CheckIsOnGround()
    {
        Vector3 origin = new Vector3(transform.position.x, transform.position.y  - (sphereCollider.radius * transform.lossyScale.y - skinwidth));
        float maxDistance = skinwidth * 2;
        RaycastHit hit;

        if (Physics.Raycast(origin, Vector3.down, out hit, maxDistance, platfomLayerMask))
        {
            if (!isOnGround)
            {
                if (animator != null)
                    animator.Play("Player_Squish", 0);
            }
            isOnGround = true;
        }
        else
            isOnGround = false;
    }

    void SetJumpSpeed()
    {
        if (isOnGround)
        {
            if (jumpTimeValueText != null)
            {
                if (!setJumpTimeValue)
                {
                    jumpTimeValueText.text = string.Format("{0}-ms", timePassedInTheAir);
                    setJumpTimeValue = true;
                }
            }
            timePassedInTheAir = 0;
        }
        else
        {
            setJumpTimeValue = false;
            timePassedInTheAir += Time.fixedDeltaTime;
        }
    }

    void Pause()
    {
        changedPauseStatus = true;
        changedResumeStatus = false;
        savedVelocity = rb.velocity;
        savedAngularVelocity = rb.angularVelocity;
        rb.isKinematic = true;
        animator.speed = 0;
    }

    void Resume()
    {
        changedPauseStatus = false;
        changedResumeStatus = true;
        rb.isKinematic = false;
        rb.AddForce(savedVelocity, ForceMode.VelocityChange);
        rb.AddTorque(savedAngularVelocity, ForceMode.VelocityChange);
        animator.speed = 1;
    }

    public void GotHit()
    {

        myRenderer.enabled = false;
        if (explosionEffect != null)
            Instantiate(explosionEffect, transform.position, transform.rotation);

        if (!raisedGameOver)
        {
            raisedGameOver = true;
            LevelManager.Instance.RaiseGameOver();
        }
    }

    public void Reset()
    {
        raisedGameOver = false;
        myRenderer.enabled = true;
        transform.position = startPosition;
        animator.Play("Player_Idle", 0);
    }

    void OnDestroy()
    {
        LevelManager.Instance.resetEvent -= Reset;
    }
    #endregion
}
