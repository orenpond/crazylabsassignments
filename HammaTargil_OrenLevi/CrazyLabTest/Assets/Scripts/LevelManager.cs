﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    #region Fields
    public Camera mainCamera;
    public BallMovement player;
    public MovingPlatforms movingPlatform;
    List<SwiningPlatform> swiningPlatforms;
    static LevelManager instance;
    bool isPausing;
    public float ViewportGuiStartYPoint = 0.7f;
    public GameObject pausePanel;
    public event Action resetEvent;
    public Text gameOverText;
    #endregion

    #region Properties
    public static LevelManager Instance
    {
        get
        {
            return instance;
        }
    }
    public bool IsPausing
    {
        get { return isPausing; }
    }
    public SwiningPlatform[] SwiningPlatforms
    {
        get { return swiningPlatforms.ToArray(); }
    }
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }

    void Initialize()
    {
        instance = new LevelManager();
        if (player == null)
            Debug.LogError("player object is null you need to set it");
        else
            instance.player = player;
        if (mainCamera == null)
            Debug.LogError("mainCamera object is null you need to set it");
        else
            instance.mainCamera = mainCamera;
        if (pausePanel == null)
            Debug.LogError("pausePanel object is null you need to set it");
        else
            instance.pausePanel = pausePanel;

        if (movingPlatform == null)
            Debug.LogError("movingPlatform object is null you need to set it");
        else
            instance.movingPlatform = movingPlatform;

        if (gameOverText == null)
            Debug.LogError("gameOverText object is null you need to set it");
        else
            instance.gameOverText = gameOverText;

        SwiningPlatform[] tempSwiningPlatformArray = FindObjectsOfType<SwiningPlatform>();
        instance.swiningPlatforms = new List<SwiningPlatform>();
        instance.swiningPlatforms.AddRange(tempSwiningPlatformArray);

        instance.ViewportGuiStartYPoint = ViewportGuiStartYPoint;
    }

    public void PressPause()
    {
        instance.isPausing = !instance.isPausing;
        if(instance.IsPausing)
            instance.pausePanel.SetActive(true);
        else
            instance.pausePanel.SetActive(false);
    }

    public void RaiseGameOver()
    {
        //instance.isPausing = true;
        // Show game over screen
        instance.gameOverText.enabled = true;
    }

    public void Reset()
    {
        instance.gameOverText.enabled = false;
        if (instance.resetEvent != null)
            instance.resetEvent();
    }
    #endregion
}
