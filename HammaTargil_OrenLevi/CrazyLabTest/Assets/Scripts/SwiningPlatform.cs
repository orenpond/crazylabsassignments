﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwiningPlatform : MonoBehaviour
{
    #region Fields
    public float rotationSpeed;
    Quaternion startRotation;
    #endregion

    #region Methods
    void Start()
    {
        startRotation = transform.rotation;
        LevelManager.Instance.resetEvent += Reset;
    }

    void FixedUpdate()
    {
        if (!LevelManager.Instance.IsPausing)
        {
            transform.Rotate(0, rotationSpeed * Time.fixedDeltaTime, 0);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!LevelManager.Instance.IsPausing)
        {
            BallMovement player = other.GetComponent<BallMovement>();
            if (player != null)
                player.GotHit();
        }
    }

    public void Reset()
    {
        transform.rotation = startRotation;
    }

    void OnDestroy()
    {
        LevelManager.Instance.resetEvent -= Reset;
    }
    #endregion
}
