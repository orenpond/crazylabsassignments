﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    #region Fields
    public Image button_released;
    public Image button_pressed;
    public UnityEvent pointerUp;
    public UnityEvent pointerDown;
    #endregion
    #region Methods
    void Start()
    {
        Image[] images = GetComponentsInChildren<Image>();
        button_released = images[0];
        button_pressed = images[1];
        button_released.enabled = true;
        button_pressed.enabled = false;
    }


    public virtual void OnPointerDown(PointerEventData ped)
    {
        PointerDown();
    }

    void PointerDown()
    {
        button_released.enabled = false;
        button_pressed.enabled = true;
        pointerDown.Invoke();
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        PointerUp();
    }

    void PointerUp()
    {
        button_released.enabled = true;
        button_pressed.enabled = false;
        pointerUp.Invoke();
    }
    #endregion
}
