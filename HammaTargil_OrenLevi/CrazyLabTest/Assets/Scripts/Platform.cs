﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    #region Fields
    BoxCollider boxCollider;
    #endregion

    #region Properties
    public Vector3 Size
    {
        get
        {
            if(boxCollider != null)
                return boxCollider.bounds.size;
            return Vector3.zero;
        }
    }
    #endregion

    #region Methods
    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }
    #endregion
}
