﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCube : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (!LevelManager.Instance.IsPausing)
        {
            BallMovement player = other.GetComponent<BallMovement>();
            if (player != null)
                player.GotHit();
        }
    }
}
