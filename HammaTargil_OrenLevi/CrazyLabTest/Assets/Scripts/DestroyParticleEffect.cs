﻿using UnityEngine;

public class DestroyParticleEffect : MonoBehaviour
{
    #region Fields
    private ParticleSystem _particleSystem;
    #endregion

    #region Methods
    public void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public void Update()
    {
        if (_particleSystem.isPlaying)
            return;

        if (_particleSystem.isPaused)
            return;

        Destroy(gameObject);
    }

    #endregion
}
