﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBlock : MonoBehaviour
{
    #region Fields
    public GameObject startPoint; // Need to be at platform block position !!!
    public GameObject endPoint;
    #endregion

    #region Methods
    void Awake()
    {
        MeshRenderer startRenderer = startPoint.GetComponent<MeshRenderer>();
        MeshRenderer endRenderer = endPoint.GetComponent<MeshRenderer>();
        startRenderer.enabled = false;
        endRenderer.enabled = false;
    }
    #endregion
}
