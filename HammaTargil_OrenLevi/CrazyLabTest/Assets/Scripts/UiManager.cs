﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class UiManager : MonoBehaviour
{
    #region Fields
    // Panels
    public GameObject panelBallSettings;
    public GameObject panelPlatfomSettings;
    public GameObject panelSwingySettings;
    public GameObject panelDebugPanel;

    // For Ball Settings
    public Dropdown ballSettings_ControlMethod;
    public Toggle ballSettings_MoveOnGroundOnly;
    public InputField ballSettings_MaxTimeForSwipe;
    public InputField ballSettings_FS_HorizontalMoveSpeed;
    public InputField ballSettings_3S_HorizontalMoveSpeed;
    public InputField ballSettings_JumpMagnitude;
    public InputField ballSettings_GravityMultiUp;
    public InputField ballSettings_GravityMultiDown;
    public InputField ballSettings_BallRightPosition;
    public InputField ballSettings_BallLeftPosition;
    public InputField ballSettings_MinimumSlideDistance;
    public Toggle ballSettings_ViewJumpTime;
    public Button ballSettings_DefaultButton;
    public Button ballSettings_SaveButton;

    // For Platform Settings
    public Toggle platformSettings_LoopButton;
    public InputField platformSettings_MovingSpeed;
    public Button platformSettings_DefaultButton;
    public Button platformSettings_SaveButton;

    // For Swingy Settings
    public InputField swingySettings_RotateSpeed;
    public Button swingySettings_DefaultButton;
    public Button swingySettings_aveButton;

    // For UiManager :)
    BallSettings ballDefaultSettings;
    BallSettings ballCurrentSettings;
    PlatformSettings platformDefaultSettings;
    PlatformSettings platformCurrentSettings;
    SwingySettings swingyDefaultSettings;
    SwingySettings swingyCurrentSettings;
    #endregion

    #region Enums
    enum SettingType { Ball, Platform, Swingy };
    #endregion

    #region Methods
    // For Pause Panel
    public void Pause_OnClickRestartButton()
    {
        LevelManager.Instance.Reset();
    }
    public void Pause_OnClickBackToMenuButton()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    } 
    public void Pause_OnClickBallSettingsButton()
    {
        if (!panelBallSettings.activeSelf)
        {
            panelBallSettings.SetActive(true);
        }
        else
        {
            panelBallSettings.SetActive(false);
        }
        panelPlatfomSettings.SetActive(false);
        panelSwingySettings.SetActive(false);
    }
    public void Pause_OnClickPlatfomSettingsButton()
    {
        if (!panelPlatfomSettings.activeSelf)
        {
            panelPlatfomSettings.SetActive(true);
        }
        else
        {
            panelPlatfomSettings.SetActive(false);
        }
        panelBallSettings.SetActive(false);
        panelSwingySettings.SetActive(false);
    }
    public void Pause_OnClickSwingySettingsButton()
    {
        if (!panelSwingySettings.activeSelf)
        {
            panelSwingySettings.SetActive(true);
        }
        else
        {
            panelSwingySettings.SetActive(false);
        }
        panelBallSettings.SetActive(false);
        panelPlatfomSettings.SetActive(false);
    }
    public void Pause_OnClickHideSettingsButton()
    {
        panelBallSettings.SetActive(false);
        panelPlatfomSettings.SetActive(false);
        panelSwingySettings.SetActive(false);
    }

    // For Ball Settings
    public void BallSettings_OnValueChangedControlMethod()
    {
        if (ballSettings_ControlMethod.value == 0)
        {
            LevelManager.Instance.player.controlMethod = BallMovement.ControlMethod.FollowSwipe;
            LevelManager.Instance.player.horizontalMoveSpeed = float.Parse(ballSettings_FS_HorizontalMoveSpeed.text);
            ballCurrentSettings.controlMethod = 0;
        }
        if (ballSettings_ControlMethod.value == 1)
        {
            LevelManager.Instance.player.controlMethod = BallMovement.ControlMethod.ThreeSwipe;
            LevelManager.Instance.player.horizontalMoveSpeed = float.Parse(ballSettings_3S_HorizontalMoveSpeed.text);
            ballCurrentSettings.controlMethod = 1;
        }
    }
    public void BallSettings_OnValueChangedMoveOnGroundOnly()
    {
        if (ballSettings_MoveOnGroundOnly.isOn)
            LevelManager.Instance.player.threeSwipeEnableOnGroundOnly = true;
        else
            LevelManager.Instance.player.threeSwipeEnableOnGroundOnly = false;
        ballCurrentSettings.moveOnGroundOnly = ballSettings_MoveOnGroundOnly.isOn;
    }
    public void BallSettings_OnEndEditMaxTimeForSwipe()
    {
        LevelManager.Instance.player.threeSwipeOnGroundOnlySensitivity = 
            float.Parse(ballSettings_MaxTimeForSwipe.text);
        ballCurrentSettings.maxTimeForSwipe = LevelManager.Instance.player.threeSwipeOnGroundOnlySensitivity;
    }
    public void BallSettings_OnEndEdit_FS_HorizontalMoveSpeed()
    {
        LevelManager.Instance.player.horizontalMoveSpeed =
            float.Parse(ballSettings_FS_HorizontalMoveSpeed.text);
        ballCurrentSettings.fS_HorizontalMoveSpeed = LevelManager.Instance.player.horizontalMoveSpeed;
    }
    public void BallSettings_OnEndEdit_3S_HorizontalMoveSpeed()
    {
        LevelManager.Instance.player.horizontalMoveSpeed =
            float.Parse(ballSettings_3S_HorizontalMoveSpeed.text);
        ballCurrentSettings._3S_HorizontalMoveSpeed = LevelManager.Instance.player.horizontalMoveSpeed;
    }
    public void BallSettings_OnEndEditJumpMagnitude()
    {
        LevelManager.Instance.player.jumpMagnitude =
            float.Parse(ballSettings_JumpMagnitude.text);
        ballCurrentSettings.jumpMagnitude = LevelManager.Instance.player.jumpMagnitude;
    }
    public void BallSettings_OnEndEditGravityMultiUp()
    {
        LevelManager.Instance.player.gravityMultiplerUpwards =
            float.Parse(ballSettings_GravityMultiUp.text);
        ballCurrentSettings.gravityMultiUp = LevelManager.Instance.player.gravityMultiplerUpwards;
    }
    public void BallSettings_OnEndEditGravityMultiDown()
    {
        LevelManager.Instance.player.gravityMultiplerDownwards =
            float.Parse(ballSettings_GravityMultiDown.text);
        ballCurrentSettings.gravityMultiDown = LevelManager.Instance.player.gravityMultiplerDownwards;
    }
    public void BallSettings_OnEndEditBallRightPosition()
    {
        LevelManager.Instance.player.threeSwipeXBallRightPosition =
            float.Parse(ballSettings_BallRightPosition.text);
        ballCurrentSettings.ballRightPosition = LevelManager.Instance.player.threeSwipeXBallRightPosition;
    }
    public void BallSettings_OnEndEditBallLeftPosition()
    {
        LevelManager.Instance.player.threeSwipeXBallLeftPosition =
            float.Parse(ballSettings_BallLeftPosition.text);
        ballCurrentSettings.ballLeftPosition = LevelManager.Instance.player.threeSwipeXBallLeftPosition;
    }
    public void BallSettings_OnEndEditMinimumSlideDistance()
    {
        LevelManager.Instance.player.slideSensitivity = 
            float.Parse(ballSettings_MinimumSlideDistance.text);
        ballCurrentSettings.minimumSlideDistance = LevelManager.Instance.player.slideSensitivity;
    }
    public void BallSettings_OnValueChangedViewJumpTime()
    {
        if (ballSettings_ViewJumpTime.isOn)
            panelDebugPanel.SetActive(true);
        else
            panelDebugPanel.SetActive(false);
        ballCurrentSettings.viewJumpTime = ballSettings_ViewJumpTime.isOn;
    }
    public void BallSettings_OnClickDefaultButton()
    {
        ballCurrentSettings.CopyOtherSettingsValue(ballDefaultSettings);
        SetUIValuesFromCurrentSettings(SettingType.Ball);
        SetAllBallsValuesFromCurrentSettings();
    }
    public void BallSettings_OnClickSaveButton()
    {
        saveXML(SettingType.Ball);
    }

    // For Platform Settings
    public void PlatformSettings_OnValueChangedLoopButton()
    {
        if (platformSettings_LoopButton.isOn)
            LevelManager.Instance.movingPlatform.moveInLoop = true;
        else
            LevelManager.Instance.movingPlatform.moveInLoop = false;
        platformCurrentSettings.enableLoop = platformSettings_LoopButton.isOn;
    }
    public void PlatformSettings_OnEndEditMovingSpeed()
    {
        LevelManager.Instance.movingPlatform.platformSpeedPerSecond =
            float.Parse(platformSettings_MovingSpeed.text);
        platformCurrentSettings.movingSpeed = LevelManager.Instance.movingPlatform.platformSpeedPerSecond;
    }
    public void PlatformSettings_OnClickDefaultButton()
    {
        platformCurrentSettings.CopyOtherSettingsValue(platformDefaultSettings);
        SetUIValuesFromCurrentSettings(SettingType.Platform);
        SetAllPlatformsValuesFromCurrentSettings();
    }
    public void PlatformSettings_OnClickSaveButton()
    {
        saveXML(SettingType.Platform);
    }

    // For Swingy Settings
    public void SwingySettings_OnEndEditRotateSpeed()
    {
        foreach (SwiningPlatform swingy in LevelManager.Instance.SwiningPlatforms)
            swingy.rotationSpeed = float.Parse(swingySettings_RotateSpeed.text);
        swingyCurrentSettings.rotateSpeed = float.Parse(swingySettings_RotateSpeed.text);
    }
    public void SwingySettings_OnClickDefaultButton()
    {
        swingyCurrentSettings.CopyOtherSettingsValue(swingyDefaultSettings);
        SetUIValuesFromCurrentSettings(SettingType.Swingy);
        SetAllSwingysValuesFromCurrentSettings();
    }
    public void SwingySettings_OnClickSaveButton()
    {
        saveXML(SettingType.Swingy);
    }

    // For UiManager :)
    void SetDefaultSettings()
    {
        if (ballDefaultSettings == null)
            ballDefaultSettings = new BallSettings();
        if (platformDefaultSettings == null)
            platformDefaultSettings = new PlatformSettings();
        if (swingyCurrentSettings == null)
            swingyDefaultSettings = new SwingySettings();

        ballDefaultSettings.ballLeftPosition = -3;
        ballDefaultSettings.ballRightPosition = 3;
        ballDefaultSettings.controlMethod = 0;
        ballDefaultSettings.fS_HorizontalMoveSpeed = 0.7f;
        ballDefaultSettings.gravityMultiDown = 1.1f;
        ballDefaultSettings.gravityMultiUp = 1;
        ballDefaultSettings.jumpMagnitude = 7;
        ballDefaultSettings.maxTimeForSwipe = 0.4f;
        ballDefaultSettings.minimumSlideDistance = 0.1f;
        ballDefaultSettings.moveOnGroundOnly = false;
        ballDefaultSettings.viewJumpTime = false;
        ballDefaultSettings._3S_HorizontalMoveSpeed = 13;

        platformDefaultSettings.enableLoop = true;
        platformDefaultSettings.movingSpeed = -10;

        swingyDefaultSettings.rotateSpeed = 25;
    }

    void SetUIValuesFromCurrentSettings(SettingType settingType)
    {
        if (settingType == SettingType.Ball)
        {
            ballSettings_ControlMethod.value = ballCurrentSettings.controlMethod;
            ballSettings_MoveOnGroundOnly.isOn = ballCurrentSettings.moveOnGroundOnly;
            ballSettings_MaxTimeForSwipe.text = ballCurrentSettings.maxTimeForSwipe.ToString();
            ballSettings_FS_HorizontalMoveSpeed.text = ballCurrentSettings.fS_HorizontalMoveSpeed.ToString();
            ballSettings_3S_HorizontalMoveSpeed.text = ballCurrentSettings._3S_HorizontalMoveSpeed.ToString();
            ballSettings_JumpMagnitude.text = ballCurrentSettings.jumpMagnitude.ToString();
            ballSettings_GravityMultiUp.text = ballCurrentSettings.gravityMultiUp.ToString();
            ballSettings_GravityMultiDown.text = ballCurrentSettings.gravityMultiDown.ToString();
            ballSettings_BallRightPosition.text = ballCurrentSettings.ballRightPosition.ToString();
            ballSettings_BallLeftPosition.text = ballCurrentSettings.ballLeftPosition.ToString();
            ballSettings_MinimumSlideDistance.text = ballCurrentSettings.minimumSlideDistance.ToString();
            ballSettings_ViewJumpTime.isOn = ballCurrentSettings.viewJumpTime;
        }
        if (settingType == SettingType.Platform)
        {
            platformSettings_LoopButton.isOn = platformCurrentSettings.enableLoop;
            platformSettings_MovingSpeed.text = platformCurrentSettings.movingSpeed.ToString();
        }
        if (settingType == SettingType.Swingy)
        {
            swingySettings_RotateSpeed.text = swingyCurrentSettings.rotateSpeed.ToString();
        }
    }

    void SetAllBallsValuesFromCurrentSettings()
    {
        if (ballCurrentSettings.controlMethod == 0)
        {
            LevelManager.Instance.player.controlMethod = BallMovement.ControlMethod.FollowSwipe;
            LevelManager.Instance.player.horizontalMoveSpeed = ballCurrentSettings.fS_HorizontalMoveSpeed;
        }
        if (ballCurrentSettings.controlMethod == 1)
        {
            LevelManager.Instance.player.controlMethod = BallMovement.ControlMethod.ThreeSwipe;
            LevelManager.Instance.player.horizontalMoveSpeed = ballCurrentSettings._3S_HorizontalMoveSpeed;
        }
        LevelManager.Instance.player.threeSwipeEnableOnGroundOnly = ballCurrentSettings.moveOnGroundOnly;
        LevelManager.Instance.player.threeSwipeOnGroundOnlySensitivity = ballCurrentSettings.maxTimeForSwipe;
        LevelManager.Instance.player.jumpMagnitude = ballCurrentSettings.jumpMagnitude;
        LevelManager.Instance.player.gravityMultiplerUpwards = ballCurrentSettings.gravityMultiUp;
        LevelManager.Instance.player.gravityMultiplerDownwards = ballCurrentSettings.gravityMultiDown;
        LevelManager.Instance.player.threeSwipeXBallRightPosition = ballCurrentSettings.ballRightPosition;
        LevelManager.Instance.player.threeSwipeXBallLeftPosition = ballCurrentSettings.ballLeftPosition;
        LevelManager.Instance.player.slideSensitivity = ballCurrentSettings.minimumSlideDistance;
    }

    void SetAllPlatformsValuesFromCurrentSettings()
    {
        LevelManager.Instance.movingPlatform.moveInLoop = platformCurrentSettings.enableLoop;
        LevelManager.Instance.movingPlatform.platformSpeedPerSecond = platformCurrentSettings.movingSpeed;
    }

    void SetAllSwingysValuesFromCurrentSettings()
    {
        foreach (SwiningPlatform swingy in LevelManager.Instance.SwiningPlatforms)
            swingy.rotationSpeed = swingyCurrentSettings.rotateSpeed;
    }

    void SetCurrentSettingsFromUIValues()
    {
        ballCurrentSettings.controlMethod = ballSettings_ControlMethod.value;
        ballCurrentSettings.moveOnGroundOnly = ballSettings_MoveOnGroundOnly.isOn;
        ballCurrentSettings.maxTimeForSwipe = float.Parse(ballSettings_MaxTimeForSwipe.text);
        ballCurrentSettings.fS_HorizontalMoveSpeed = float.Parse(ballSettings_FS_HorizontalMoveSpeed.text);
        ballCurrentSettings._3S_HorizontalMoveSpeed = float.Parse(ballSettings_3S_HorizontalMoveSpeed.text);
        ballCurrentSettings.jumpMagnitude = float.Parse(ballSettings_JumpMagnitude.text);
        ballCurrentSettings.gravityMultiUp = float.Parse(ballSettings_GravityMultiUp.text);
        ballCurrentSettings.gravityMultiDown = float.Parse(ballSettings_GravityMultiDown.text);
        ballCurrentSettings.ballRightPosition = float.Parse(ballSettings_BallRightPosition.text);
        ballCurrentSettings.ballLeftPosition = float.Parse(ballSettings_BallLeftPosition.text);
        ballCurrentSettings.minimumSlideDistance = float.Parse(ballSettings_MinimumSlideDistance.text);
        ballCurrentSettings.viewJumpTime = ballSettings_ViewJumpTime.isOn;

        platformCurrentSettings.enableLoop = platformSettings_LoopButton.isOn;
        platformCurrentSettings.movingSpeed = float.Parse(platformSettings_MovingSpeed.text);

        swingyCurrentSettings.rotateSpeed = float.Parse(swingySettings_RotateSpeed.text);
    }

    void Start()
    {
        SetDefaultSettings();

        ballCurrentSettings = new BallSettings();
        platformCurrentSettings = new PlatformSettings();
        swingyCurrentSettings = new SwingySettings();

        if (checkAllXmlFiles() == false)
        {
            ballCurrentSettings.CopyOtherSettingsValue(ballDefaultSettings);
            platformCurrentSettings.CopyOtherSettingsValue(platformDefaultSettings);
            swingyCurrentSettings.CopyOtherSettingsValue(swingyDefaultSettings);
            saveAllXml();
        }
        else
        {
            loadAllXml();
        }
        SetUIValuesFromCurrentSettings(SettingType.Ball);
        SetUIValuesFromCurrentSettings(SettingType.Platform);
        SetUIValuesFromCurrentSettings(SettingType.Swingy);
        SetAllBallsValuesFromCurrentSettings();
        SetAllPlatformsValuesFromCurrentSettings();
        SetAllSwingysValuesFromCurrentSettings();
    }

    // File Managment
    bool checkAllXmlFiles()
    {
        if (!checkXmlFile(SettingType.Ball))
            return false;
        if (!checkXmlFile(SettingType.Platform))
            return false;
        if (!checkXmlFile(SettingType.Swingy))
            return false;
        return true;
    }

    bool saveAllXml()
    {
        if (!saveXML(SettingType.Ball))
            return false;
        if (!saveXML(SettingType.Platform))
            return false;
        if (!saveXML(SettingType.Swingy))
            return false;
        return true;
    }

    bool loadAllXml()
    {
        if (!loadXML(SettingType.Ball))
            return false;
        if (!loadXML(SettingType.Platform))
            return false;
        if (!loadXML(SettingType.Swingy))
            return false;
        return true;
    }

    bool checkXmlFile(SettingType settingType)
    {
        if(settingType == SettingType.Ball)
            return File.Exists(Application.persistentDataPath + "/BallSettings.xml");
        if (settingType == SettingType.Platform)
            return File.Exists(Application.persistentDataPath + "/PlatformSettings.xml");
        if (settingType == SettingType.Swingy)
            return File.Exists(Application.persistentDataPath + "/SwingySettings.xml");
        return false;
    }

    bool saveXML(SettingType settingType)
    {
        XmlSerializer serializer = null;
        if(settingType == SettingType.Ball)
            serializer = new XmlSerializer(typeof(BallSettings));
        if (settingType == SettingType.Platform)
            serializer = new XmlSerializer(typeof(PlatformSettings));
        if (settingType == SettingType.Swingy)
            serializer = new XmlSerializer(typeof(SwingySettings));

        try
        {
            Debug.Log(Application.persistentDataPath);
            string fileName = "";
            if (settingType == SettingType.Ball)
                fileName = "/BallSettings.xml";
            if (settingType == SettingType.Platform)
                fileName = "/PlatformSettings.xml";
            if (settingType == SettingType.Swingy)
                fileName = "/SwingySettings.xml";

            using (FileStream stream = new FileStream(Application.persistentDataPath + fileName, FileMode.Create))
            {
                using (StreamWriter sWriter = new StreamWriter(stream, Encoding.UTF8))
                {
                    if (settingType == SettingType.Ball)
                        serializer.Serialize(sWriter, ballCurrentSettings);
                    if (settingType == SettingType.Platform)
                        serializer.Serialize(sWriter, platformCurrentSettings);
                    if (settingType == SettingType.Swingy)
                        serializer.Serialize(sWriter, swingyCurrentSettings);
                    //stream.Close();
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            return false;
        }
        return true;
    }

    bool loadXML(SettingType settingType)
    {
        XmlSerializer serializer = null;
        if (settingType == SettingType.Ball)
            serializer = new XmlSerializer(typeof(BallSettings));
        if (settingType == SettingType.Platform)
            serializer = new XmlSerializer(typeof(PlatformSettings));
        if (settingType == SettingType.Swingy)
            serializer = new XmlSerializer(typeof(SwingySettings));
        try
        {
            string fileName = "";
            if (settingType == SettingType.Ball)
                fileName = "/BallSettings.xml";
            if (settingType == SettingType.Platform)
                fileName = "/PlatformSettings.xml";
            if (settingType == SettingType.Swingy)
                fileName = "/SwingySettings.xml";
            using (FileStream stream = new FileStream(Application.persistentDataPath + fileName, FileMode.Open))
            {
                using (StreamReader sReader = new StreamReader(stream, Encoding.UTF8)) // FOR WINDOWS
                {
                    if (settingType == SettingType.Ball)
                        ballCurrentSettings = serializer.Deserialize(sReader) as BallSettings;
                    if (settingType == SettingType.Platform)
                        platformCurrentSettings = serializer.Deserialize(sReader) as PlatformSettings;
                    if (settingType == SettingType.Swingy)
                        swingyCurrentSettings = serializer.Deserialize(sReader) as SwingySettings;
                    stream.Close();
                }
            }
        }
        catch (Exception ex)
        {
            if (settingType == SettingType.Ball)
                ballCurrentSettings.CopyOtherSettingsValue(ballDefaultSettings);
            if (settingType == SettingType.Platform)
                platformCurrentSettings.CopyOtherSettingsValue(platformDefaultSettings);
            if (settingType == SettingType.Swingy)
                swingyCurrentSettings.CopyOtherSettingsValue(swingyDefaultSettings);
            Debug.Log(ex.Message);
            return false;
        }
        return true;
    }
    #endregion

    #region Inner Classes
    [Serializable]
    public class BallSettings
    {
        public int controlMethod;
        public bool moveOnGroundOnly;
        public float maxTimeForSwipe;
        public float fS_HorizontalMoveSpeed;
        public float _3S_HorizontalMoveSpeed;
        public float jumpMagnitude;
        public float gravityMultiUp;
        public float gravityMultiDown;
        public float ballRightPosition;
        public float ballLeftPosition;
        public float minimumSlideDistance;
        public bool viewJumpTime;

        public void CopyOtherSettingsValue(BallSettings otherSettings)
        {
            controlMethod = otherSettings.controlMethod;
            moveOnGroundOnly = otherSettings.moveOnGroundOnly;
            maxTimeForSwipe = otherSettings.maxTimeForSwipe;
            fS_HorizontalMoveSpeed = otherSettings.fS_HorizontalMoveSpeed;
            _3S_HorizontalMoveSpeed = otherSettings._3S_HorizontalMoveSpeed;
            jumpMagnitude = otherSettings.jumpMagnitude;
            gravityMultiUp = otherSettings.gravityMultiUp;
            gravityMultiDown = otherSettings.gravityMultiDown;
            ballRightPosition = otherSettings.ballRightPosition;
            ballLeftPosition = otherSettings.ballLeftPosition;
            minimumSlideDistance = otherSettings.minimumSlideDistance;
            viewJumpTime = otherSettings.viewJumpTime;
        }
    }

    [Serializable]
    public class PlatformSettings
    {
        public bool enableLoop;
        public float movingSpeed;

        public void CopyOtherSettingsValue(PlatformSettings otherSettings)
        {
            enableLoop = otherSettings.enableLoop;
            movingSpeed = otherSettings.movingSpeed;
        }
    }

    [Serializable]
    public class SwingySettings
    {
        public float rotateSpeed;
        public void CopyOtherSettingsValue(SwingySettings otherSettings)
        {
            rotateSpeed = otherSettings.rotateSpeed;
        }
    }
    #endregion
}
